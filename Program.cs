﻿using NAudio.CoreAudioApi;
using NAudio.Wave;
using System;
using System.Linq;

namespace GamingForestCopyCat
{
    public class Program
    {
        private static MMDevice _sourceDevice;
        private static int _destinationDevice;

        private static WasapiLoopbackCapture _capture;
        private static WaveOutEvent _player;
        private static BufferedWaveProvider _bufferedProvider;

        private static float _volume = .8f;
        //private static int _latency = 200;

        private static string[] _startArgs;


        public static void Main(string[] args)
        {
            _startArgs = args;

            Console.Clear();
            PrintLogo();

            ResetDevices();

            RequestUserDevicePreferences();

            StartDevices();

            MainLoop();
        }
        
        private static void RequestUserDevicePreferences()
        {
            ChooseInputDevice();
            ChooseOutputDevice();
            
            if (_sourceDevice.FriendlyName.Equals(WaveOut.GetCapabilities(_destinationDevice).ProductName))
            {
                Console.Clear();
                PrintLogo();

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Input and Output Device must NOT be the same device!\n");
                Console.ResetColor();

                RequestUserDevicePreferences();
            }
        }
        private static void ChooseInputDevice()
        {
            MMDevice[] devices = new MMDeviceEnumerator().EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active).ToArray();
            
            (string, Action)[] options = new (string, Action)[devices.Length];
            for (int i = 0; i < options.Length; i++)
            {
                int idx = i;
                options[i] = (devices[i].FriendlyName, () => _sourceDevice = devices[idx]);
            }

            RequestUserInput(
                "Source Devices (loopback)",
                "Choose Source Device",
                options
            );

            Console.WriteLine($"\nCopy from: {_sourceDevice.FriendlyName}\n\n");
        }
        private static void ChooseOutputDevice()
        {
            (string, Action)[] options = new (string, Action)[WaveOut.DeviceCount];
            for (int i = 0; i < options.Length; i++)
            {
                int idx = i;
                options[i] = (WaveOut.GetCapabilities(i).ProductName, () => _destinationDevice = idx);
            }

            RequestUserInput(
                "Destination Devices (ASIO)",
                "Choose Destination Device",
                options
            );

            Console.WriteLine($"\nPaste to: {options[_destinationDevice].Item1}\n\n");
        }

        private static void ResetDevices()
        {
            if (_player != null)
            {
                _player.Stop();
                _player.Dispose();
                _player = null;
            }

            if (_capture != null)
            {
                _capture.StopRecording();
                _capture.Dispose();
                _capture = null;
            }

            if (_bufferedProvider != null)
            {
                _bufferedProvider.ClearBuffer();
                _bufferedProvider = null;
            }
        }
        private static void StartDevices()
        {
            if (_capture != null || _bufferedProvider != null || _player != null)
                ResetDevices();

            _capture = new WasapiLoopbackCapture(_sourceDevice);
            _capture.DataAvailable += OnDataAvailable;

            _bufferedProvider = new BufferedWaveProvider(_capture.WaveFormat);
            //_bufferedProvider.BufferDuration = TimeSpan.FromMilliseconds(_latency);
            _bufferedProvider.DiscardOnBufferOverflow = true;

            _player = new WaveOutEvent();
            _player.DeviceNumber = _destinationDevice;
            _player.Init(_bufferedProvider);
            _player.Volume = _volume;
            //_player.DesiredLatency = _latency;

            _capture.StartRecording();
            _player.Play();
        }

        private static void PrintStatsHeader()
        {
            Console.SetCursorPosition(0, 0);

            PrintLogo();

            string inputDevice = "-";
            string outputDevice = "-";
            string volume = "-";
            string samplerate = "-";
            string channels = "-";
            string bits = "-";

            if (_player != null && _player.PlaybackState == PlaybackState.Playing)
            {
                inputDevice = _sourceDevice.FriendlyName;
                outputDevice = WaveOut.GetCapabilities(_destinationDevice).ProductName;
                volume = $"{Math.Round(_volume * 100, 0)}%";
                samplerate = _player.OutputWaveFormat.SampleRate.ToString();
                channels = _player.OutputWaveFormat.Channels.ToString();
                bits = _player.OutputWaveFormat.BitsPerSample.ToString();
            }

            Console.WriteLine($"Copy from '{inputDevice}'  |  Paste to '{outputDevice}'");
            Console.WriteLine($"Volume: {volume,4} | Samplerate: {samplerate,6} | Channels: {channels,2} | Bits: {bits,2}");
            Console.WriteLine();
        }
        private static void PrintLogo()
        {
            Console.WriteLine(@"  G a m i n g F o r e s t   C o p y C a t  ");
            Console.WriteLine(@"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            Console.WriteLine(@"      ´ `  /\___/\   =   /\___/\  ´ `      ");
            Console.WriteLine(@"     /  / /  `  ´ \  =  / `  ´  \ \  \     ");
            Console.WriteLine(@"    /  / (   b   b \ = / d   d   ) \  \    ");
            Console.WriteLine(@"    \  |  \   ~^~  / = \  ~^~   /  |  /    ");
            Console.WriteLine(@"     \  \  `> - - <  =  > - - <´  /  /     ");
            Console.WriteLine(@"      |  | /  | | |  =  | | |  \  | |      ");
            Console.WriteLine(@"      \__`´`,_| |_`, = ,´_| |_,´`´__/      ");
            Console.WriteLine(@"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        }

        private static void MainLoop()
        {
            Console.Clear();
            PrintStatsHeader();

            RequestUserInput(
                "Menu", 
                "Choose Action",
                ("Restart", () => Main(_startArgs)),
                ("Settings", SettingsLoop),
                ("Quit", Quit)
            );
        }
        private static void SettingsLoop()
        {
            Console.Clear();
            PrintStatsHeader();

            RequestUserInput(
                    "Settings",
                    "Choose Action",
                    ("Volume", VolumeLoop),
                    //("Latency", LatencyLoop),
                    ("Back", MainLoop)
            );
        }
        private static void VolumeLoop()
        {
            Console.Clear();
            PrintStatsHeader();

            RequestUserInput(
                "Set Volume",
                () => { return $"Volume: {Math.Round(_player.Volume * 100, 0),3}%"; },
                () => _player.Volume = Math.Clamp(_player.Volume + .05f, 0f, 1f), 
                () => _player.Volume = Math.Clamp(_player.Volume - .05f, 0f, 1f)
            );

            _volume = _player.Volume;

            SettingsLoop();
        }
        //private static void LatencyLoop()
        //{
        //    Console.Clear();

        //    RequestUserInput(
        //        $"Set Latency (currently: {_latency}ms)", 
        //        "Latency", 
        //        5, 
        //        8000,
        //        out _latency
        //    );

        //    StartDevices();

        //    SettingsLoop();
        //}

        private static void OnDataAvailable(object sender, WaveInEventArgs e)
        {
            _bufferedProvider.AddSamples(e.Buffer, 0, e.BytesRecorded);
        }

        private static void RequestUserInput(string header, string prompt, params (string name, Action action)[] options)
        {
            bool validInput = false;
            int parsedIndex = -1;

            Console.WriteLine(header);

            for (int i = 0; i < options.Length; i++)
                Console.WriteLine($"{i,2}.) {options[i].name}");

            Console.WriteLine("- - - - - - - - - - - - - - - - - - - - - -");


            while (!validInput)
            {
                Console.Write($"{prompt}: ");

                validInput = int.TryParse(Console.ReadKey().KeyChar.ToString(), out parsedIndex)
                    && parsedIndex >= 0
                    && parsedIndex < options.Length;

                if (!validInput)
                    Console.CursorLeft = 0;
            }

            options[parsedIndex].action.Invoke();
        }
        private static void RequestUserInput(string header, Func<string> prompt, Action onIncrease, Action onDecrease)
        {
            Console.WriteLine(header);
            Console.WriteLine("Increase or Decrease using the Up/Down arrows on your keyboard.");
            Console.WriteLine("Return using Enter/Escape.");
            Console.WriteLine("- - - - - - - - - - - - - - - - - - - - - -");

            while (true)
            {
                Console.Write(prompt.Invoke());

                ConsoleKey key = Console.ReadKey(true).Key;

                switch (key)
                {
                    case ConsoleKey.UpArrow:
                        onIncrease();
                        break;

                    case ConsoleKey.DownArrow:
                        onDecrease();
                        break;

                    case ConsoleKey.Escape:
                    case ConsoleKey.Enter:
                        return;
                }

                Console.CursorLeft = 0;
            }
        }
        private static void RequestUserInput(string header, string prompt, int minValue, int maxValue, out int result)
        {
            Console.WriteLine(header);
            Console.WriteLine($"Enter whole number between {minValue} and {maxValue}.");
            Console.WriteLine("- - - - - - - - - - - - - - - - - - - - - -");

            result = -1;

            bool validInput = false;
            while (!validInput)
            {
                Console.Write($"{prompt}: ");

                validInput = int.TryParse(Console.ReadLine(), out result)
                    && result >= minValue
                    && result <= maxValue;

                if (!validInput)
                {
                    Console.CursorLeft = 0;
                    Console.CursorTop -= 1;
                }
            }
        }

        private static void Quit()
        {
            ResetDevices();

            Environment.Exit(0);
        }
    }
}
